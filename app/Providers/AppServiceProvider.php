<?php

namespace App\Providers;


use App\Elastic\Elastic;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Elastic::class, function ($app) {

            return new Elastic(
                ClientBuilder::create()
                    ->setHosts([
                        'elasticsearch:9200'
                    ])
                    //->setLogger(ClientBuilder::(storage_path('logs/elastic.log')))
                    ->build()
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function boot()
    {
        $elastic = $this->app->make(Elastic::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Elastic::class];
    }
}
