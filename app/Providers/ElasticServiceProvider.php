<?php

namespace App\Providers;


use App\Elastic\Elastic;
use Elasticsearch\Client;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;

class ElasticServiceProvider extends ServiceProvider
{
    /**
     * ElasticServiceProvider constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
    }

    /**
     * Search document in elasticsearch database
     *
     * @param string $index
     * @param $type
     * @param string $searchString
     * @param Elastic $client
     * @return array
     */
    public function searchDocument(string $index, string $type, string $searchString, Elastic $client): array
    {
        $query = [
            'query_string' => [
                'query' => '*' . $searchString . '*',
                'fields' => ['name', 'login'],
            ],
        ];

        $parameters = [
            'index' => $index,
            'type' => $type,
            'body' => [
                "from" => 0,
                "size" => 20,
                'query' => $query
            ]
        ];

        return $client->search($parameters);
    }

    /**
     * Create document in elasticsearch database
     *
     * @param $index
     * @param $type
     * @param $content
     * @param Elastic $client
     * @return array
     */
    public function createDocument(string $index, string $type, array $content, Elastic $client): array
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'body' => [
                'name' => $content['name'],
                'login' => $content['login']
            ]
        ];

        return $client->index($params);
    }
}
