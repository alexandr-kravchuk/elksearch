<?php

namespace App\Http\Controllers;

use App\Elastic\Elastic;
use App\Providers\ElasticServiceProvider;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ElasticController extends Controller
{
    /**
     * Index document on elasticsearch.
     */
    const CUSTOMERS = 'customers';

    /**
     * Type document on elasticsearch.
     */
    const TYPE = 'my_local_test_app';

    /**
     * @var Elastic
     */
    private $elasticClient;

    /**
     * @var ElasticServiceProvider
     */
    private $elasticServiceProvider;

    /**
     * ElasticController constructor.
     * @param ElasticServiceProvider $elasticServiceProvider
     * @param Elastic $elasticClient
     */
    public function __construct(ElasticServiceProvider $elasticServiceProvider, Elastic $elasticClient)
    {
        $this->elasticServiceProvider = $elasticServiceProvider;
        $this->elasticClient = $elasticClient;
    }

    /**
     * Create document method in elasticsearch.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addDocument(Request $request)
    {
        $errors = [];
        $result = [];

        $content = json_decode($request->getContent(), true);

        if (isset($content['name']) && isset($content['login'])) {
            try {
                $result = $this->elasticServiceProvider->createDocument(self::CUSTOMERS, self::TYPE, $content, $this->elasticClient);
            } catch (\Exception $exception) {
                $error[] = $exception->getMessage();
            }
        } else {
            $errors[] = 'Empty {name} or/and {login} parameters';
        }

        return response()->json([
            'errors' => $errors,
            'result' => $result
        ]);
    }

    /**
     * Query string search in elasticsearch db.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function searchDocument(Request $request): JsonResponse
    {
        $errors = [];
        $result = [];

        if (isset($request['query'])) {
            $queryString = $request['query'];
            try {
                $result = $this->elasticServiceProvider->searchDocument(self::CUSTOMERS, self::TYPE, $queryString, $this->elasticClient);
                if (isset($result['hits']['hits'])) {
                    $result = $result['hits']['hits'];
                }
            } catch (\Exception $exception) {
                $errors[] = $exception->getMessage();
            }
        } else {
            $errors[] = 'Empty {query} parameter';
        }

        $accounts = [];
        if (!$errors) {
            foreach ($result as $item) {
                $account = [];
                $account['value'] = $item['_source']['name'];
                $account['data'] = $item['_source']['login'];
                $account['src'] = 'img/_src/710.png';
                $accounts[] = $account;
            }
        }

        return response()->json($accounts);
    }
}
